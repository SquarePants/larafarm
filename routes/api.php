<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('farmers', 'App\Http\Controllers\FarmerController@index');
Route::get('farmers/{id}', 'App\Http\Controllers\FarmerController@show');
Route::post('farmers', 'App\Http\Controllers\FarmerController@store');
Route::get('forests', 'App\Http\Controllers\ForestController@index');
Route::get('forests/{id}', 'App\Http\Controllers\ForestController@show');
Route::post('forests', 'App\Http\Controllers\ForestController@store');
Route::put('forests/plant/{id}', 'App\Http\Controllers\ForestController@plantTree');
Route::put('forests/collect/{id}', 'App\Http\Controllers\ForestController@collectWood');
Route::put('forests/build/{id}', 'App\Http\Controllers\ForestController@buildHouse');
