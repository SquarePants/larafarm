<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forest extends Model
{
    use HasFactory;
    protected $fillable = [
        'forest_name',
        'tree_count',
        'wood_count',
        'house_count',
    ];
    public function farmer(){
        return $this->belongsTo('App\Models\Farmer');
    }
}
