<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farmer extends Model
{
    use HasFactory;
    protected $fillable = [
        'location',
        'farmer_name',
        'forest_id',
    ];
    public function forest(){
        return $this->belongsTo("App\Models\Forest");
    }
}
