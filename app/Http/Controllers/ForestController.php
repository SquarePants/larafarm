<?php

namespace App\Http\Controllers;

use App\Models\Farmer;
use App\Models\Forest;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ForestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forests = Forest::all();
        return response()->json([
            'success' => true,
            'data' => $forests,
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $forest = new Forest([
            'forest_name' => $request->get('forest_name'),
            'tree_count' => $request->get('tree_count'),
            'wood_count' => $request->get('wood_count'),
            'house_count' => $request->get('house_count'),
        ]);
        $forest->save();
        return response()->json([
            'success' => true,
            'data' => $forest,
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $farmer =  Farmer::with('forest')->findOrFail($id);
            return response()->json([
                'success' => true,
                'data' => $farmer->forest
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
                dd($e);
        }
    }
    
    public function plantTree($id){
        Forest::where('id', $id)
        ->increment('tree_count',1);
        return response()->json([
            'success' => true
        ], Response::HTTP_OK);
    }
    
    public function collectWood($id){
        Forest::where('id', $id)
        ->increment('wood_count',1);
        Forest::where('id',$id)
        ->decrement('tree_count',5);
        
        return response()->json([
            'success' => true
        ], Response::HTTP_OK);
    }

    public function buildHouse($id){
        Forest::where('id', $id)
        ->increment('house_count',1);
        Forest::where('id', $id)
        ->decrement('wood_count',1000);
        
        return response()->json([
            'success' => true
        ], Response::HTTP_OK);
    }

   
}
