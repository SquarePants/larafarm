<?php

namespace App\Http\Controllers;

use App\Models\Farmer;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FarmerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmers = Farmer::all();
        return response()->json([
            'success' => true,
            'data' => $farmers,
        ], Response::HTTP_OK);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $farmer = new Farmer([
            'farmer_name' => $request->get('farmer_name'),
            'location' => $request->get('location'),
            'forest_id' => $request->get('forest_id'),
        ]);
        $farmer->save();
        return response()->json([
            'success' => true,
            'data' => $farmer,
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $farmer = Farmer::findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $farmer,
        ], Response::HTTP_OK);
    }

    
}
